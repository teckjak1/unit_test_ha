package com.nac.demoapi;

import android.os.AsyncTask;
import android.util.Log;

public final class MTask<T> extends AsyncTask<Object, Object, Object> {
    private static final String TAG = MTask.class.getName();
    private OnAsyncCallBack callBack;
    private int key;
    private OnActionCallBack<T> rsCallBack;

    public MTask(OnAsyncCallBack callBack, OnActionCallBack<T> rsCallBack, int key) {
        this.key = key;
        this.callBack = callBack;
        this.rsCallBack = rsCallBack;
    }

    @Override
    protected final void onPreExecute() {
        callBack.initTask();
    }

    @Override
    protected Object doInBackground(Object... data) {
        CommonUtil.logI(TAG, "doInBackground..." + key);
        return callBack.execTask(this, key, data);
    }

    public final void updateTask(Object... data) {
        publishProgress(data);
    }

    @Override
    protected final void onProgressUpdate(Object... values) {
        callBack.updateUI(key, values);
    }

    @Override
    protected final void onPostExecute(Object result) {
        callBack.taskComplete(key, rsCallBack, result);
    }

    public final void start(Object... data) {
        execute(data);
    }

    public final void startAsync(Object... data) {
        executeOnExecutor(THREAD_POOL_EXECUTOR, data);
    }

    public interface OnAsyncCallBack {
        default void initTask() {
        }

        Object execTask(MTask task, int key, Object... data);

        default void updateUI(int key, Object... values) {
        }

        default <T> void taskComplete(int key, OnActionCallBack<T> rsCallBack, Object result) {
            rsCallBack.callBack(key, (T) result);
        }
    }
}
