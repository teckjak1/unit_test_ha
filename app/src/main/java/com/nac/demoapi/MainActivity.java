package com.nac.demoapi;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.nac.demoapi.entities.ListEmployeeEntity;
import com.nac.demoapi.presenter.HomePresenter;

import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MTask.OnAsyncCallBack {

    private static final String TAG = MainActivity.class.getName();
    private static final int KEY_GET_ALL_EMPLOYEE = 101;
    private HomePresenter mHomePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
    }

    private void initViews() {
        findViewById(R.id.bt_get_all).setOnClickListener(this);
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 101);
        } else {
            mHomePresenter = new HomePresenter();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mHomePresenter = new HomePresenter();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_get_all) {
            new MTask<>(this, (OnActionCallBack<Response<ListEmployeeEntity>>) (key, rs) -> {
                if (rs.isSuccessful() && rs.body() != null) {
                    ListEmployeeEntity data = rs.body();
                    CommonUtil.logI(TAG, "ListEmployeeEntity: " + data);
                } else {
                    CommonUtil.logI(TAG, "Error: " + rs.code() + ":" + rs.message());
                }
            }, KEY_GET_ALL_EMPLOYEE).startAsync();
        }
    }

    @Override
    public final Object execTask(MTask task, int key, Object... data) {
        try {
            if (key == KEY_GET_ALL_EMPLOYEE) {
                CommonUtil.logI(TAG, "execTask:...KEY_GET_ALL_EMPLOYEE");
                return mHomePresenter.getAllEmployee();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public final <T> void taskComplete(int key, OnActionCallBack<T> rsCallBack, Object result) {
        if (key == KEY_GET_ALL_EMPLOYEE) {
            rsCallBack.callBack(key, (T) result);
        }
    }
}
