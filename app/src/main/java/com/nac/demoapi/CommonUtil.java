package com.nac.demoapi;

import android.util.Log;

public class CommonUtil {
    public static final int LOG_TYPE_TEST = 1;
    public static final int LOG_TYPE_INFO = 2;
    public static final int LOG_TYPE_DEBUG = 3;
    private static  int logType = LOG_TYPE_TEST;

    public static void setLogType(int log){
        CommonUtil.logType = log;
    }

    public static void logI(String tag, String mes){
        if(logType == LOG_TYPE_INFO) {
            Log.i(tag, mes);
        }else if(logType == LOG_TYPE_TEST){
            System.out.println(tag + ": "+mes);
        }
    }

    public static void logD(String tag, String mes){
        if(logType == LOG_TYPE_DEBUG) {
            Log.i(tag, mes);
        }else if(logType == LOG_TYPE_TEST){
            System.out.println(tag + ": "+mes);
        }
    }
}
