package com.nac.demoapi.presenter;

import com.nac.demoapi.api.IEmployeeAPI;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class BasePresenter {
    public static final String BASE_URL = "http://dummy.restapiexample.com/api/v1/";

    public BasePresenter() {
        initAPI();
    }

    private void initAPI() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(new OkHttpClient.Builder()
                        .callTimeout(30, TimeUnit.SECONDS)
                        .hostnameVerifier((hostname, session) -> true)
                        .build())
                .build();

        createAPI (retrofit);
    }

    protected abstract void createAPI(Retrofit retrofit);
}
