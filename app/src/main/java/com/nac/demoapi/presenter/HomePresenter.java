package com.nac.demoapi.presenter;

import com.nac.demoapi.CommonUtil;
import com.nac.demoapi.api.IEmployeeAPI;
import com.nac.demoapi.entities.ListEmployeeEntity;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.Retrofit;

public class HomePresenter extends BasePresenter {

    private static final String TAG = HomePresenter.class.getName();
    private IEmployeeAPI mEmployeeAPI;

    public double calc(int key, int a, int b) {
        if (key == 1) {
            return a + b;
        } else if (key == 2) {
            return a - b;
        } else if (key == 3) {
            return a * b;
        } else if (key == 4) {
            return a * 1F / b;
        } else {
            return -1;
        }
    }

    @Override
    protected void createAPI(Retrofit retrofit) {
        mEmployeeAPI = retrofit.create(IEmployeeAPI.class);
    }


    public Response<ListEmployeeEntity> getAllEmployee() {
        CommonUtil.logI(TAG, "getAllEmployee...");
        try {
            return mEmployeeAPI.getAllEmployee().execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
