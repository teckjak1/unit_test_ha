import androidx.test.core.app.ApplicationProvider;

import com.nac.demoapi.App;
import com.nac.demoapi.CommonUtil;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.io.InputStream;

import okhttp3.mockwebserver.MockWebServer;

@Config(manifest = Config.NONE, sdk = 23, application = App.class)
@RunWith(RobolectricTestRunner.class)
public abstract class BaseTest {
    MockWebServer mockWebServer;
    App app;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        app = ApplicationProvider.getApplicationContext();

        CommonUtil.setLogType(CommonUtil.LOG_TYPE_TEST);
        mockWebServer = new MockWebServer();
        mockWebServer.start();
    }

    String getTextFile(String fileName) {
        try {
            InputStream in = getClass().getResourceAsStream("/files/" + fileName);
            byte[] buff = new byte[1024];
            int len;

            StringBuilder text = new StringBuilder();
            assert in != null;
            while ((len = in.read(buff)) > 0) {
                text.append(new String(buff, 0, len));
            }

            in.close();
            return text.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
